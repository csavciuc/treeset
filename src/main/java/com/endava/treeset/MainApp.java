package com.endava.treeset;

import com.endava.treeset.model.Student;
import com.endava.treeset.set.StudentSet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.Arrays;


@SpringBootApplication
public class MainApp {

    public static void main(String[] args) {
        StudentSet treeSet = new StudentSet();
        Student firstStudent = Student.builder()
                .name("Ann")
                .details("Details")
                .dateOfBirth(LocalDate.of(2007, 8, 10)).build();
        Student secondStudent = Student.builder()
                .name("Ann")
                .details("Details")
                .dateOfBirth(LocalDate.of(2009, 10, 12)).build();
        Student thirdStudent = Student.builder()
                .name("Mary")
                .details("Details")
                .dateOfBirth(LocalDate.of(2000, 10, 10)).build();
        Student[] students = new Student[]{firstStudent, secondStudent, thirdStudent};
        treeSet.addAll(Arrays.asList(students));

        if (treeSet.contains(firstStudent)) {
            System.out.println("TreeSet contains the element with name: " + firstStudent.getName() + "\n");
        } else {
            System.out.println("TreeSet doesn't contain the element with name: " + firstStudent.getName());
        }

        System.out.print("Tree set elements: \n");

        treeSet.forEach(student -> {
            System.out.println(student.getName() + " is " + student.age() + " years old");
        });

        treeSet.remove(treeSet.stream().findFirst().get());

        Student[] students2 = new Student[]{firstStudent, secondStudent};
        treeSet.retainAll(Arrays.asList(students2));

        System.out.print("\nTree set after retain: \n");
        treeSet.forEach(student -> {
            System.out.println(student.getName());
        });

        treeSet.removeAll(Arrays.asList(students));
        System.out.println("\nTreeSet is empty: " + treeSet.isEmpty());
        SpringApplication.run(MainApp.class, args);
    }
}
