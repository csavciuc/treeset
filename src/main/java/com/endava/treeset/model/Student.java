package com.endava.treeset.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Comparator;

import static java.time.temporal.ChronoUnit.YEARS;
import static java.util.Comparator.reverseOrder;

@Builder
@Getter
@Setter
public class Student implements Comparable<Student> {
    private String name;
    private LocalDate dateOfBirth;
    private String details;

    public Student(String name, LocalDate dateOfBirth, String details) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.details = details;
    }

    public int age() {
        LocalDate now = LocalDate.now();
        LocalDate birthDate = this.dateOfBirth;
        return (int) YEARS.between(birthDate, now);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Student) {
            var object = (Student) obj;
            boolean isNameEquals = this.name.equalsIgnoreCase(object.name);
            boolean isDateOfBirthEquals = this.dateOfBirth.equals(object.dateOfBirth);
            return isNameEquals & isDateOfBirthEquals;
        }
        return false;
    }

    public int compareTo(Student student) {
        Comparator<Student> studentComparator = Comparator.comparing(Student::getName)
                .thenComparing(Student::age, reverseOrder());
        return studentComparator.compare(this, student);
    }
}
