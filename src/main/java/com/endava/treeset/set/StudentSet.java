package com.endava.treeset.set;

import com.endava.treeset.model.Student;

import java.util.*;

public class StudentSet implements Set<Student> {

    private final Map<Student, Integer> map = new HashMap<>();

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object object) {
        return map.containsKey(object);
    }

    @Override
    public Iterator<Student> iterator() {
        var items = new ArrayList<>(map.keySet());
        Collections.sort(items);
        return items.iterator();
    }

    @Override
    public Object[] toArray() {
        return map.keySet().toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        return array;
    }

    @Override
    public boolean add(Student student) {
        if (map.containsKey(student)) {
            return false;
        }

        map.put(student, student.age());
        return true;
    }

    @Override
    public boolean remove(Object object) {
        if (!map.containsKey(object)) {
            return false;
        }
        map.remove(object);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        for (Object object : collection) {
            if (object instanceof Student) {
                if (!contains(object)) {
                    return false;
                }
            } else {
                throw new ClassCastException();
            }
        }

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Student> collection) {
        collection.forEach(this::add);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        if (null == collection) {
            return false;
        }

        Iterator<Student> it = iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        if (null == collection) {
            return false;
        }

        Iterator<?> it = iterator();
        while (it.hasNext()) {
            if (collection.contains(it.next())) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        map.clear();
    }
}
